﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TestApp.Models
{
    public class NotificationRepository : IRepository<Notification>
    {
        private readonly TestDbContext context;
        private DbSet<Notification> notifications;
        public NotificationRepository()
        {
            this.context = new TestDbContext();
        }
        private DbSet<Notification> Notifications
        {
            get
            {
                if (notifications == null)
                {
                    notifications = context.Set<Notification>();
                }
                return notifications;
            }
        }
        public virtual IQueryable<Notification> List
        {
            get { return this.Notifications; }
        }

        public void Create(Notification notification)
        {
            if (notification == null)
            {
                throw new ArgumentNullException("notification");
            }
            this.Notifications.Add(notification);
            context.SaveChanges();
        }

        public void Delete(Notification notification)
        {
            if (notification == null)
            {
                throw new ArgumentNullException("notification");
            }
            this.Notifications.Remove(notification);
            this.context.SaveChanges();
        }

        public Notification GetById(int id)
        {
            return this.Notifications.Include(x=>x.Users).ToList().Find(x=>x.Id==id);
        }

        public void Update(Notification notification)
        {
            if (notification == null)
            {
                throw new ArgumentNullException("notification");
            }
            var entity = this.GetById(notification.Id);
            entity.Name = notification.Name;
            entity.TemplateMessage = notification.TemplateMessage;
            context.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                    this.context.Dispose();
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~NotificationRepository() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}