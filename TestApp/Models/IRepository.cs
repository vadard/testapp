﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Models
{
   public interface IRepository<T>:IDisposable where T:BaseEntity
    {
        T GetById(int id);
        void Create(T entity);
        void Delete(T entity);
        void Update(T entity);
        IQueryable<T> List { get; }
    }
}
