﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace TestApp.Models
{
    public class Notification:BaseEntity
    {
        public Notification()
        {
            Users = new HashSet<User>();
        }
        [Required]
        [MaxLength(20)]
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Шаблон сообщения")]
        public string TemplateMessage { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}