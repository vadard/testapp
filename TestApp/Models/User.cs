﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace TestApp.Models
{
    public class User : BaseEntity
    {
        public User()
        {
            Notifications = new HashSet<Notification>();
        }
        [Required]
        [MaxLength(100)]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(100)]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [Required]
        [MaxLength(100)]
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Уведомления")]
        public ICollection<Notification> Notifications { get; set; }
        [NotMapped]
        public int[] SelectedNotifications { get; set; }

    }
}