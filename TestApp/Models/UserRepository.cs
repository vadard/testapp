﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TestApp.Models
{
    public class UserRepository : IRepository<User>
    {
        private readonly TestDbContext context;
        private IDbSet<User> users;
        public UserRepository()
        {
            this.context = new TestDbContext();
        }
        private IDbSet<User> Users
        {
            get
            {
                if (users == null)
                {
                    users = context.Set<User>();
                }
                return users;
            }
        }
        public void Create(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            foreach (var notification in context.Notifications.ToList())
            {
                if (user.SelectedNotifications.Contains(notification.Id))
                {
                    user.Notifications.Add(notification);
                }
            }
            this.Users.Add(user);
            context.SaveChanges();
        }
        public void Delete(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            this.Users.Remove(user);
            this.context.SaveChanges();
        }
        public void Update(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            var entity = GetById(user.Id);
            entity.Notifications.Clear();
            foreach (var notification in context.Notifications.ToList())
            {
                if (user.SelectedNotifications.Contains(notification.Id))
                {
                    entity.Notifications.Add(notification);
                }
            }
            entity.LastName = user.LastName;
            entity.FirstName = user.FirstName;
            entity.MiddleName = user.MiddleName;
            entity.Email = user.Email;
            context.SaveChanges();
        }
        public User GetById(int id)
        {
            var user = this.Users.Include(x=>x.Notifications).ToList().Find(x=>x.Id==id);
            if (user != null)
            {
                user.SelectedNotifications = user.Notifications.Select(x => x.Id).ToArray();
            }
            return this.Users.Find(id);
        }
        public virtual IQueryable<User> List
        {
            get { return this.Users; }
        }
        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.context.Dispose();
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~UserRepository() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}