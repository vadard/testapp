﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestApp.Models;
using System.Text;
namespace TestApp
{
    public class NotificationService
    {
        public void SendNotification(Notification notification, IMailer mailer)
        {
            foreach (var user in notification.Users)
            {
                StringBuilder template = new StringBuilder(notification.TemplateMessage);
                template = template.Replace("[UserFirstName]", user.FirstName);
                template = template.Replace("[UserLastName]", user.LastName);
                template = template.Replace("[UserMiddleName]", user.MiddleName);

                mailer.Send(user.Email, template.ToString());
            }
        }
    }
}