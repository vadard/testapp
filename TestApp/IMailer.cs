﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Models;

namespace TestApp
{
    public interface IMailer
    {
        void Send(string mail,string message);
        
    }
}
