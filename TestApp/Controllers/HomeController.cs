﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestApp.Models;
namespace TestApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<Notification> notificationRepository;
        private readonly IMailer mailer;
       private readonly NotificationService service;
        public HomeController()
        {
            this.notificationRepository = new NotificationRepository();
            this.mailer = new BestMailer();
            this.service = new NotificationService();
        }
        public ActionResult Index()
        {
          return View(notificationRepository.List);
        }
        public string Send(int id)
        {
            var notification = notificationRepository.GetById(id);
            service.SendNotification(notification, new BestMailer());
            return $"Уведомление {notification.Name} отправлено!";
        }
    }
}